DECLARE 
   dept_id number; 
   no_of_employees number;
   min_salary number;
   CURSOR function is
   SELECT DEPARTMENT_ID, count(DEPARTMENT_ID) FROM HR.employees group by DEPARTMENT_ID HAVING count(DEPARTMENT_ID) < 50;
BEGIN
   OPEN function; 
   LOOP 
   FETCH function into dept_id, no_of_employees;
      EXIT WHEN function%notfound;
      SELECT min(salary) into min_salary FROM HR.employees where department_id = dept_id;
      insert_vacancy(dept_id, 50-no_of_employees, min_salary);
   END LOOP; 
   CLOSE function;