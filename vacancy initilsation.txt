CREATE OR REPLACE PROCEDURE insert_vacancy(department_id IN HR.EMPLOYEES.DEPARTMENT_ID%type, v_available IN number, min_salary IN number)
AS 
BEGIN 
   INSERT INTO vacancy(dept_id, v_available, min_salary) values(department_id, v_available, min_salary);
END; 